import { Routes } from '@angular/router';
// import { ShowTraineeshipComponent } from './show-traineeship/show-traineeship.component';
// import { FormTraineeshipComponent } from './form-traineeship/form-traineeship.component';
// import { ShowOneTraineeshipComponent } from './show-one-traineeship/show-one-traineeship.component';
import { ShowLessonComponent } from './show-lesson/show-lesson.component';

export const LessonRoutes: Routes = [
    {
      path: '',
      children: [ {
        path: 'show',
        component: ShowLessonComponent
    }]} 
  //   ,{
  //     path: ':id',
  //     component:ShowOneTraineeshipComponent
  // },
];
