import { Component, OnInit } from '@angular/core';
import { LessonService } from 'src/app/repository/lesson.service';

@Component({
  selector: 'app-show-lesson',
  templateUrl: './show-lesson.component.html',
  styleUrls: ['./show-lesson.component.css']
})
export class ShowLessonComponent implements OnInit {

  constructor(private lessons:LessonService) { }

  ngOnInit() {
  }

}
