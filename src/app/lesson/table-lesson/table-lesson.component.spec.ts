import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TableLessonComponent } from './table-lesson.component';

describe('TableLessonComponent', () => {
  let component: TableLessonComponent;
  let fixture: ComponentFixture<TableLessonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TableLessonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TableLessonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
