import { Component, OnInit, Input } from '@angular/core';
import swal from 'sweetalert2';
import { LessonService } from 'src/app/repository/lesson.service';

@Component({
  selector: 'app-table-lesson',
  templateUrl: './table-lesson.component.html',
  styleUrls: ['./table-lesson.component.css']
})
export class TableLessonComponent implements OnInit {

  constructor(private lesson:LessonService ) { }
  @Input()
  tableData1
  @Input()
  lessons

  users: any;

  //   consoleTest()
  // {
  //   console.log(this.traineeships); 
  // } 
  ngOnInit() {

    // this.addTrai.push(this.traineeships);

    this.tableData1 = {
      headerRow: ['#', 'Name', 'nombre de personnes', 'date début', 'date fin', 'Prix', 'Actions'],
      dataRows: this.lessons
    }


  };

  consoleTest() {

    console.log(this.users);
  }



  onRemove(traineeship) {
    // this.data.splice(this.data.indexOf(msg), 1);
    // this.currentUser.professionUsers.splice(professionUser, 1);
    const index: number = this.lessons.indexOf(traineeship);
    if (index !== -1) {
      this.lessons.splice(index, 1);
    }
  };

  deleteMessage(traineeship) {

    swal({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonClass: 'btn btn-success',
      cancelButtonClass: 'btn btn-danger',
      confirmButtonText: 'Yes, delete it!',
      buttonsStyling: false
    }).then((result) => {
      if (result.value) {
        this.deleteTraineeship(traineeship.id);
        this.onRemove(traineeship);
      }
    })
  }
  deleteTraineeship(traineeship) {

    this.lessons.deleteTraineeshipByProfessionnal(traineeship).subscribe(

      () => swal(

        {
          title: 'Deleted!',
          text: 'Your file has been deleted.',
          type: 'success',
          confirmButtonClass: "btn btn-success",
          buttonsStyling: false
        }
      )
    )


  }

}
