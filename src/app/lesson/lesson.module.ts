import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NouisliderModule } from 'ng2-nouislider';
import { TagInputModule } from 'ngx-chips';
import { SelectModule } from 'ng2-select';
import { MaterialModule } from '../app.module';
import { LessonRoutes } from './lesson.routing';



import { ShowLessonComponent } from './show-lesson/show-lesson.component';
import { OneLessonComponent } from './one-lesson/one-lesson.component';
import { TableLessonComponent } from './table-lesson/table-lesson.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(LessonRoutes),
    FormsModule,
    ReactiveFormsModule,
    NouisliderModule,
    TagInputModule,
    MaterialModule,

    
  ],
  declarations: [
     
    ShowLessonComponent,
    OneLessonComponent,
    TableLessonComponent
  ]
})

export class LessonModule {}
