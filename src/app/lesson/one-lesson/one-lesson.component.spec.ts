import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OneLessonComponent } from './one-lesson.component';

describe('OneLessonComponent', () => {
  let component: OneLessonComponent;
  let fixture: ComponentFixture<OneLessonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OneLessonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OneLessonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
