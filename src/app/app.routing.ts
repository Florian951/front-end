import { Routes } from '@angular/router';
import { AuthLayoutComponent } from './layouts/auth/auth-layout.component';
import { AdminLayoutComponent } from './layouts/admin/admin-layout.component';
import { AuthGuard } from './auth.guard';
import { Role } from './entity/role';


export const AppRoutes: Routes = [
    {
      path: '',
      redirectTo: 'dashboard',
      pathMatch: 'full',
    }, {
      path: '',
      component: AdminLayoutComponent,
      children: [
          {
        path: '',
        loadChildren: './dashboard/dashboard.module#DashboardModule'
    }, {
        path: 'components',
        loadChildren: './components/components.module#ComponentsModule'
    }, {
        path: 'forms',
        loadChildren: './forms/forms.module#Forms'
    }, {
        path: 'tables',
        loadChildren: './tables/tables.module#TablesModule'
    }, {
        path: 'maps',
        loadChildren: './maps/maps.module#MapsModule'
    }, {
        path: 'widgets',
        loadChildren: './widgets/widgets.module#WidgetsModule'
    }, {
        path: 'charts',
        loadChildren: './charts/charts.module#ChartsModule'
    }, {
        path: 'calendar',
        loadChildren: './calendar/calendar.module#CalendarModule'
    },{
        path: 'profile',
        loadChildren: './profile/profile.module#ProfileModule'
    },{
        path: 'competences',
        loadChildren: './competences/competences.module#CompetencesModule'
    },{
        path: '',
        loadChildren: './userpage/user.module#UserModule'
    }, {
        path: '',
        loadChildren: './timeline/timeline.module#TimelineModule'
    }, {
        path: 'calendrier',
        loadChildren: './calendar-user/calendar-user.module#CalendarUserModule'
    }, {
        path: 'traineeship',
        loadChildren: './traineeship/traineeship.module#TraineeshipModule'
    }, {
        path: 'lesson',
        loadChildren: './lesson/lesson.module#LessonModule'
    }, {
        path: 'client',
        loadChildren: './client/client.module#ClientModule'
    }
    ]}, {
        path: '',
        component: AuthLayoutComponent,
        children: [{
          path: 'pages',
          loadChildren: './pages/pages.module#PagesModule'
        }]
      }
  
  
];
