import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class UploadService {
  private url = this.getUrlApi();
  
  constructor(private http:HttpClient) { }

  getUrlApi(): string {
    return globalThis.customEnvironment.api.url;
  };

  findDocument(idTraineeship) {
    return this.http.get<[]>(this.url+'/upload/find/'+ idTraineeship );
  }

  deleteDocument(documentId:number) {
    return this.http.delete<[]>(this.url+'/upload/delete/'+ documentId);
  }
}
