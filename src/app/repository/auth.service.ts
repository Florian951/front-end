import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from '../entity/User';
import { tap, switchMap } from "rxjs/operators";
import { Observable, BehaviorSubject } from 'rxjs';
import { Router } from '@angular/router';
import { Address } from '../entity/Address';
import { Profession } from '../entity/Profession';



@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private url =  this.getUrlApi();


  public user: BehaviorSubject<User> = new BehaviorSubject(null);

  constructor(private http: HttpClient, private router: Router) { }

  getUrlApi():string{
    return globalThis.customEnvironment.api.url;
  };

  addUser(user: User) {
    return this.http.post<User>(this.url + '/register', user);
  }

  addUserPro(user: User) {
    return this.http.post<User>(this.url + '/register-pro', user);
  }

  addAddressUser(address: Address) {
    return this.http.post<Address>(this.url + '/address', address);
  }

  addAddressHorse(address: Address) {
    return this.http.post<Address>(this.url + '/address', address);
  }

  findAddressHorse(): Observable<Address[]> {
    return this.http.get<Address[]>(this.url + '/address/find-horse');
  }

  patchAddAddressUser(userAddress: Address) {
    return this.http.patch<Address>(this.url + '/address' + userAddress.id, userAddress);
  }

  userPatch(currentUser: User, user) {
    return this.http.patch<User>(this.url + '/register/' + currentUser.id, user);
  }

  particulierPatch(currentUser: User, user){
    return this.http.patch<User>(this.url + '/password-patch/' + currentUser.id, user);
  }

  login(username: string, password: string) {
    return this.http.post<{ token: string }>(this.url +'/login_check', {
      username,
      password
    }).pipe(
      tap(data => localStorage.setItem('token', data.token)),
      switchMap(() => this.getUser())
    );
  }

  getToken(): string {
    return localStorage.getItem('token');
  }

  checkMail(mail) {
    return this.http.get<User>(this.url + '/' + mail)
  }
  
  findAllOldClientForPro(): Observable<User[]>{
    return this.http.get<User[]>(this.url + '/find-old-users');
  }

  findAllUser(): Observable<User[]> {
    return this.http.get<User[]>(this.url + '/findAllUser');
  }

  findAllProfessionForPro(): Observable<Profession[]>{
    return this.http.get<Profession[]>(this.url + '/profession');
  }

  logout() {
    localStorage.removeItem('token');
    this.user.next(null);
  }


  getUser(): Observable<User> {
    return this.http.get<User>(this.url + "/user").pipe(
      //On utilise l'opérateur pipe pour faire en sorte de pousser le user
      //dans le Subject au moment où l'observable récupère la réponse http
      tap(user => this.user.next(user))
    );
  }

}
