import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Lesson } from '../entity/Lesson';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LessonService {


  private url = this.getUrlApi();

  constructor(private http:HttpClient) { }

  getUrlApi(): string {
    return globalThis.customEnvironment.api.url;
  };
 
  validationLessonByProfessionnal(eventCalendarByPro) {
    return this.http.post<Lesson>(this.url+'/add', eventCalendarByPro);
  }

  registerLessonForParticular(traineeshipId) {
    return this.http.get(this.url+'/register-particular/'+ traineeshipId);
  }

  deleteLessonByProfessionnal(traineeship:Lesson) {
    return this.http.delete<Lesson>(this.url +"/delete/" + traineeship); 
  }

  findOneLesson(id:Lesson) {
    return this.http.get<Lesson[]>(this.url +"/find-one/" + id); 
  }
}
