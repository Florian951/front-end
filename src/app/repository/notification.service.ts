import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Notification } from '../entity/Notification';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {
  private url = this.getUrlApi();

  constructor(private http: HttpClient) { }

  getUrlApi(): string {
    return globalThis.customEnvironment.api.url;
  };

  getNotificationNotRead() {
    return this.http.get<Notification[]>(this.url +"/notification/search"); 
  }
  patchNotificationNotRead(notification:Notification) {
    return this.http.patch<Notification>(this.url +'/notification/read/'+ notification.id , notification); 
  }
}
