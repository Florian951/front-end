import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Profession } from '../entity/Profession';
import { ProfessionUser } from '../entity/ProfessionUser';

@Injectable({
  providedIn: 'root'
})
export class CompetencesService {

  constructor(private http: HttpClient) { }
  private url = this.getUrlApi();

  getUrlApi():string{
    return globalThis.customEnvironment.api.url;
  };

  findAllProfessionForPro(): Observable<[]> {
    return this.http.get<[]>(this.url + '/profession');
  }

  AddProfessionPro(profession: ProfessionUser, professionSelected) {
    let params = new HttpParams();
    params = params.append('profession', professionSelected);

    return this.http.post<ProfessionUser>(this.url + '/profession/add', profession, { params: params });
  }

  deleteCompetenceByProfessionnal(traineeship: ProfessionUser) {
    return this.http.delete<ProfessionUser>(this.url + "/profession/delete/" + traineeship);
  }

 
}
