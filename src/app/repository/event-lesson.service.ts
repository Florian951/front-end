import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CalendarEvent } from '../entity/CalendarEvent';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EventLessonService {
  private url = this.getUrlApi();

  constructor(private http: HttpClient) { }
  //add event pour le professionnel

  getUrlApi(): string {
    return globalThis.customEnvironment.api.url;
  };

  addEventForProfessionnal(eventCalendarByPro) {
    return this.http.post<CalendarEvent>(this.url + '/add-Event-Calendar', eventCalendarByPro);
  }

  findAllEventForParticular($id): Observable<CalendarEvent[]> {
    return this.http.get<CalendarEvent[]>(this.url +  '/search-event/' + $id);
  }

  findAllEventByUser(): Observable<CalendarEvent[]> {
    return this.http.get<CalendarEvent[]>(this.url + '/find-lesson');
  }
}
