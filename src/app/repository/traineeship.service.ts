import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Traineeship } from '../entity/Traineeship';
import { Observable } from 'rxjs';
import { Address } from '../entity/Address';


@Injectable({
  providedIn: 'root'
})
export class TraineeshipService {


  private url = this.getUrlApi();

  constructor(private http:HttpClient) { }

  getUrlApi(): string {
    return globalThis.customEnvironment.api.url;
  };
 
  addTraineeshipByProfessionnal(eventCalendarByPro) {
    return this.http.post<Traineeship>(this.url+'/traineeship/add', eventCalendarByPro);
  }

  registerTraineeshipForParticular(traineeshipId) {
    return this.http.get(this.url+'/traineeship/register-particular/'+ traineeshipId);
  }

  findAllEventByUser(): Observable<Traineeship[]>{
    return this.http.get<Traineeship[]>(this.url+ '/traineeship/find-all');
  }

  deleteTraineeshipByProfessionnal(traineeship:Traineeship) {
    return this.http.delete<Traineeship>(this.url +"/traineeship/delete/" + traineeship); 
  }

  findOneTraineeship(id:Traineeship) {
    return this.http.get<Traineeship[]>(this.url +"/traineeship/find-one/" + id); 
  }

  addAddressTraineeship(address, traineeshipId) {
    let params = new HttpParams();
    params = params.append('traineeship', traineeshipId);

    return this.http.post<Address>(this.url +'/address/traineeship', address, {params: params}); 
  }
}
