import { Component, OnInit, ElementRef, OnDestroy, Input } from '@angular/core';
import { AuthService } from 'src/app/repository/auth.service';
import { Router } from '@angular/router';
import { User } from 'src/app/entity/User';

declare var $: any;


@Component({
    selector: 'app-login-cmp',
    templateUrl: './login.component.html'
})

export class LoginComponent implements OnInit{

    username:string;
    password:string;
    message:string;
    currentUser;  
    btnText = 'pas encore de compte?';
  
    
    test: Date = new Date();
    private toggleButton: any;
    private sidebarVisible: boolean;
    private nativeElement: Node;

    constructor(private element: ElementRef, private repoAuth:AuthService, private router:Router) {
        this.nativeElement = element.nativeElement;
        this.sidebarVisible = false;
    }

    ngOnInit() {
    
        
        var navbar : HTMLElement = this.element.nativeElement;
        this.toggleButton = navbar.getElementsByClassName('navbar-toggle')[0];
        const body = document.getElementsByTagName('body')[0];
        body.classList.add('login-page');
        body.classList.add('off-canvas-sidebar');
        const card = document.getElementsByClassName('card')[0];
        setTimeout(function() {
            // after 1000 ms we add the class animated to the login/register card
            card.classList.remove('card-hidden');
        }, 700);
    }
    sidebarToggle() {
        var toggleButton = this.toggleButton;
        var body = document.getElementsByTagName('body')[0];
        var sidebar = document.getElementsByClassName('navbar-collapse')[0];
        if (this.sidebarVisible == false) {
            setTimeout(function() {
                toggleButton.classList.add('toggled');
            }, 500);
            body.classList.add('nav-open');
            this.sidebarVisible = true;
        } else {
            this.toggleButton.classList.remove('toggled');
            this.sidebarVisible = false;
            body.classList.remove('nav-open');
        }
    }
    changeText() {
        if(this.btnText === 'pas encore de compte?') {
          this.btnText = '';
        }else {
          this.btnText = 'pas encore de compte?';
        }
      }
     ngOnDestroy(){
       const body = document.getElementsByClassName('registerBtn')[0];
       body.classList.add('d-none');
     }

     login() {
        this.repoAuth.login(this.username, this.password).subscribe(
          () => this.router.navigate(['pages/home']),
          data => this.message = 'Authentication Error.'
        )
      }
    
      logout() {
        this.repoAuth.logout();
        this.message = '';
        this.router.navigate(['/pages/home']);
      }
    
}
