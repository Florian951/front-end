import { Routes } from '@angular/router';
import { RegisterComponent } from './register/register.component';
import { PricingComponent } from './pricing/pricing.component';
import { LockComponent } from './lock/lock.component';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { ProfilComponent} from './profil/profil.component';
import { MapsComponent } from "./profil/maps/maps.component";
import { ProfilParticulierComponent } from './profil-particulier/profil-particulier.component';
import { RegisterProComponent } from './register-pro/register-pro.component';
import { Role } from '../entity/role';
import { AuthGuard } from '../auth.guard';


export const PagesRoutes: Routes = [

    {
        path: '',
        children: [ {
            path: 'login',
            component: LoginComponent
        }, {
            path: 'lock',
            component: LockComponent
        }, {
            path: 'pricing',
            component: PricingComponent
        },{
            path: 'home',
            component: HomeComponent,
            // canActivate: [AuthGuard],
            // data: { role: ['ROLE_PRO'] } 
        },{
            path: 'Particulier',
            component:ProfilParticulierComponent,
             canActivate: [AuthGuard],
            data: { role: ['ROLE_PARTICULIER'] }
        },{
            path: 'registerPro',
            component:RegisterProComponent
        },{
            path: ':id',
            component:ProfilComponent
        }

    ]
    }
];
