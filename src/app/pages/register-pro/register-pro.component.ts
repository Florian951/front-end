import { Component, OnInit ,Input} from '@angular/core';
import { FormGroup, FormArray, FormControl, FormBuilder } from '@angular/forms';
import { User } from 'src/app/entity/User';
import { AuthService } from 'src/app/repository/auth.service';
import swal from 'sweetalert2';
import { Profession } from 'src/app/entity/Profession';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register-pro',
  templateUrl: './register-pro.component.html',
  styleUrls: ['./register-pro.component.css']
})
export class RegisterProComponent implements OnInit {

  professionData: Profession[] = [
    { name: 'Coach',optionProfession: null, optionHour: null, picture:null },
    { name: 'osteopathe',optionProfession: null, optionHour: null, picture:null },
    { name: 'Dentiste',optionProfession: null, optionHour: null, picture:null  },
    { name: 'Maréchal ferrant',optionProfession: null, optionHour: null, picture:null  },
    { name: 'Pareur/Podologue',optionProfession: null, optionHour: null, picture:null  },
    { name: 'Ethologue',optionProfession: null, optionHour: null, picture:null  },
    { name: 'Shiatsu',optionProfession: null, optionHour: null, picture:null  }
  ];

  professions: any;
  form: FormGroup;
  user: User = {
    firstName: null,
    lastName: null,
    password: null,
    twitter: null,
    webSite: null,
    facebook: null,
    email: null,
    instagram: null,
    lessonPrice: null,
    description: null,
    professionUsers: null,
  };
  message: string;
  
  @Input()
    btnRegister:string;

  constructor(private authService: AuthService, private fb: FormBuilder, private router:Router) { }

  onChange(name: string, isChecked: boolean) {
    const profession = (this.form.controls.name as FormArray);

    if (isChecked) {
      profession.push(new FormControl(name));
    } else {
      const index = profession.controls.findIndex(x => x.value === name);
      profession.removeAt(index);
    }
  }

  ngOnInit() {

    const body = document.getElementsByTagName('body')[0];
    body.classList.add('register-page');
    body.classList.add('off-canvas-sidebar');

  }

  showSwal() {

    swal({
      title: "Création de compte reussi!",
      // text: "You clicked the button!",
      buttonsStyling: false,
      confirmButtonClass: "btn btn-success",
      type: "success"
    }).catch(swal.noop)

  }

  registerPro() {

    this.authService.addUserPro(this.user).subscribe(
      () => this.router.navigate(['pages/login']),
      data => this.message = data.error.message);

  }


}
