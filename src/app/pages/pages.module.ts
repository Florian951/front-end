import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { MaterialModule } from '../app.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
// import { FlexLayoutModule } from '@angular/flex-layout';
import { CalendarComponent } from './calendar/calendar.component';


import { PagesRoutes } from './pages.routing';






import { RegisterComponent } from './register/register.component';
import { PricingComponent } from './pricing/pricing.component';
import { LockComponent } from './lock/lock.component';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { FooterComponent } from './footer/footer.component';
import { HttpClientModule } from '@angular/common/http';
import { ProfilComponent } from './profil/profil.component';
import { ComponentsModule } from '../components/components.module';
import { SweetAlertComponent } from '../components/sweetalert/sweetalert.component';
import { MapsComponent } from './profil/maps/maps.component';
// import { GoogleMapsComponent } from './maps/GoogleMaps.component';

import { GoogleMapsComponent } from '../maps/googlemaps/googlemaps.component';
import { ProfilParticulierComponent } from './profil-particulier/profil-particulier.component';
import { RegisterProComponent } from './register-pro/register-pro.component';
import { FormDescriptionParticulierComponent } from './profil-particulier/form-description-particulier/form-description-particulier.component';
import { InfoPersoComponent } from './profil-particulier/info-perso/info-perso.component';
import { FormSocialNetworkParticulierComponent } from './profil-particulier/form-social-network-particulier/form-social-network-particulier.component';
import { FormPasswordParticulierComponent } from './profil-particulier/form-password-particulier/form-password-particulier.component';
import { FormEquideComponent } from './profil-particulier/form-equide/form-equide.component';
import { EquideComponent } from './profil-particulier/equide/equide.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(PagesRoutes),
    FormsModule,
    MaterialModule,
    ReactiveFormsModule,
    HttpClientModule,
    MaterialModule,
  
  ],
  declarations: [
    LoginComponent,
    RegisterComponent,
    PricingComponent,
    LockComponent,
    HomeComponent,
    FooterComponent,
    ProfilComponent,
    MapsComponent,
    GoogleMapsComponent,
    ProfilParticulierComponent,
    CalendarComponent,
    RegisterProComponent,
    FormDescriptionParticulierComponent,
    InfoPersoComponent,
    FormSocialNetworkParticulierComponent,
    FormPasswordParticulierComponent,
    FormEquideComponent,
    EquideComponent,
    
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
  
})

export class PagesModule {}

