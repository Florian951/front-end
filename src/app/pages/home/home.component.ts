import { Component, OnInit, Output } from '@angular/core';
import { AuthService } from 'src/app/repository/auth.service';
import { User } from 'src/app/entity/User';
import { Profession } from 'src/app/entity/Profession';

import { ActivatedRoute } from '@angular/router';



@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
 

})
export class HomeComponent implements OnInit {

  @Output()
  users:any=[];
  professionnelsSelected;
  professions:any=[];

  constructor( private repo:AuthService,private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.repo.findAllUser().subscribe(data => this.users = data);

  }


}
