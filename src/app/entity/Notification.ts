export interface Notification {
    id?:number;
    name:string;
    creationdate:Date;
    notificationRead:boolean;
    
}