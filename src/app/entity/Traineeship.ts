export interface Traineeship {
    id?:number;
    title:string;
    start:Date;
    end:Date;
    className:string;
    program:string;
    level:string;
    price:number;
    numberParticipant:number;
}