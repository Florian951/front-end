export interface Address {
    id?:number;
    street:string;
    city:string;
    zipCode:number;
    country:string;
    adressHorse:boolean;
    department:string;
    region:string;

}