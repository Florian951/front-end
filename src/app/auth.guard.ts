import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable, of } from 'rxjs';
import { AuthService } from './repository/auth.service';
import { map, tap, delay, catchError } from 'rxjs/operators';
import { Role } from './entity/role';


@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(private authService:AuthService, private router:Router) {}


  

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
      return this.authService.getUser().pipe(
      map(user => {
        if(user === null) {
          return false;
        }
        if(route.data.role.some(r=> user.roles.includes(r))){
          
          return true;
          
        }
        return false;
      }),
      catchError(() => of(false)),
      tap(access => {
        if(!access ) {
          this.router.navigate(['pages/home']); //TODO modifier le lien dans le navigate pour mettre le path qui correspond à votre page login
        }
      })
    );
  }
  
  
}
