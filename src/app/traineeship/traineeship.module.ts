import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NouisliderModule } from 'ng2-nouislider';
import { TagInputModule } from 'ngx-chips';
import { SelectModule } from 'ng2-select';
import { MaterialModule } from '../app.module';
import { TraineeshipRoutes } from './traineeship.routing';


import { CarouselModule, WavesModule, ButtonsModule } from 'angular-bootstrap-md';



import { ShowTraineeshipComponent } from './show-traineeship/show-traineeship.component';
import { FormTraineeshipComponent } from './form-traineeship/form-traineeship.component';
import { TableTraineeshipComponent } from './table-traineeship/table-traineeship.component';
import { ShowOneTraineeshipComponent } from './show-one-traineeship/show-one-traineeship.component';
import { FormAddressTraineeshipComponent } from './form-address-traineeship/form-address-traineeship.component';
import { ShowProfilesTraineeshipComponent } from './show-profiles-traineeship/show-profiles-traineeship.component';
import { DropzonejsTraineeshipComponent } from './dropzonejs-traineeship/dropzonejs-traineeship.component';
import { DropzoneModule } from 'ngx-dropzone-wrapper';




@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(TraineeshipRoutes),
    FormsModule,
    ReactiveFormsModule,
    NouisliderModule,
    TagInputModule,
    MaterialModule,
  
    DropzoneModule, CarouselModule, WavesModule, ButtonsModule

  ],
  declarations: [

    FormTraineeshipComponent,
    ShowTraineeshipComponent,
    TableTraineeshipComponent,
    ShowOneTraineeshipComponent,
    FormAddressTraineeshipComponent,
    ShowProfilesTraineeshipComponent,
    DropzonejsTraineeshipComponent
  ]

})

export class TraineeshipModule { }
