import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TableTraineeshipComponent } from './table-traineeship.component';

describe('TableTraineeshipComponent', () => {
  let component: TableTraineeshipComponent;
  let fixture: ComponentFixture<TableTraineeshipComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TableTraineeshipComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TableTraineeshipComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
