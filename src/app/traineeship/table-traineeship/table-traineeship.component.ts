import { Component, OnInit, Input, Output } from '@angular/core';
import { TraineeshipService } from 'src/app/repository/traineeship.service';
import swal from 'sweetalert2';

@Component({
  selector: 'app-table-traineeship',
  templateUrl: './table-traineeship.component.html',
  styleUrls: ['./table-traineeship.component.css']
})

export class TableTraineeshipComponent implements OnInit {

  constructor(private traineeship: TraineeshipService) { }
  @Input()
  tableData1
  @Input()
  traineeships

  users: any;

  //   consoleTest()
  // {
  //   console.log(this.traineeships); 
  // } 
  ngOnInit() {

    // this.addTrai.push(this.traineeships);

    this.tableData1 = {
      headerRow: ['#', 'Name', 'nombre de personnes', 'date début', 'date fin', 'Prix', 'Actions'],
      dataRows: this.traineeships
    }


  };

  consoleTest() {

    console.log(this.users);
  }



  onRemove(traineeship) {
    // this.data.splice(this.data.indexOf(msg), 1);
    // this.currentUser.professionUsers.splice(professionUser, 1);
    const index: number = this.traineeships.indexOf(traineeship);
    if (index !== -1) {
      this.traineeships.splice(index, 1);
    }
  };

  deleteMessage(traineeship) {

    swal({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonClass: 'btn btn-success',
      cancelButtonClass: 'btn btn-danger',
      confirmButtonText: 'Yes, delete it!',
      buttonsStyling: false
    }).then((result) => {
      if (result.value) {
        this.deleteTraineeship(traineeship.id);
        this.onRemove(traineeship);
      }
    })
  }
  deleteTraineeship(traineeship) {

    this.traineeship.deleteTraineeshipByProfessionnal(traineeship).subscribe(

      () => swal(

        {
          title: 'Deleted!',
          text: 'Your file has been deleted.',
          type: 'success',
          confirmButtonClass: "btn btn-success",
          buttonsStyling: false
        }
      )
    )


  }

}
