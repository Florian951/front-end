import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowProfilesTraineeshipComponent } from './show-profiles-traineeship.component';

describe('ShowProfilesTraineeshipComponent', () => {
  let component: ShowProfilesTraineeshipComponent;
  let fixture: ComponentFixture<ShowProfilesTraineeshipComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShowProfilesTraineeshipComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowProfilesTraineeshipComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
