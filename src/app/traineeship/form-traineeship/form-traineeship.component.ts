import { Component, OnInit, Output } from '@angular/core';
import { Traineeship } from 'src/app/entity/Traineeship';
import { TraineeshipService } from 'src/app/repository/traineeship.service';
import swal from 'sweetalert2';

@Component({
  selector: 'app-form-traineeship',
  templateUrl: './form-traineeship.component.html',
  styleUrls: ['./form-traineeship.component.css']
})
export class FormTraineeshipComponent implements OnInit {
  priceSlider = 125;
  personSlider = 25;
  pickerFinal;
  message = null;

  traineeship:Traineeship = {
    title:null,
    start:null,
    end:null,
    className: "",
    program:null,
    price:null,
    level:null,
    numberParticipant:null
  }

  levelConfig = [
    {value: '1', viewValue: 'niveau 1'},
    {value: '2', viewValue: 'niveau 2'},
    {value: '3', viewValue: 'niveau 3'},
    {value: '4', viewValue: 'niveau 4'},
    {value: '5', viewValue: 'niveau 5'},
    {value: '6', viewValue: 'niveau 6'},
    {value: '7', viewValue: 'niveau 7'},
  ];
  constructor(private repoTraineeship:TraineeshipService) { }

   addTraineeship(){
     this.traineeship.price = this.priceSlider;
     this.traineeship.numberParticipant = this.personSlider;
     this.repoTraineeship.addTraineeshipByProfessionnal(this.traineeship).subscribe(() =>
     this.showSwal(),
     data => this.message = data);
   }

   showSwal() {

    swal({
      title: "Création d'une formation reussi!'",
      // text: "You clicked the button!",
      buttonsStyling: false,
      confirmButtonClass: "btn btn-success",
      type: "success"
      
      
    }).catch(swal.noop)
    document.addEventListener("click", function(){
      window.location.reload();
    });

  }
 consoleTest(){
   console.log(this.message);
   
 }
  ngOnInit() {
  }

}
