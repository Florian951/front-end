import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormTraineeshipComponent } from './form-traineeship.component';

describe('FormTraineeshipComponent', () => {
  let component: FormTraineeshipComponent;
  let fixture: ComponentFixture<FormTraineeshipComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormTraineeshipComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormTraineeshipComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
