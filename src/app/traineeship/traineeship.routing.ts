import { Routes } from '@angular/router';
import { ShowTraineeshipComponent } from './show-traineeship/show-traineeship.component';
import { FormTraineeshipComponent } from './form-traineeship/form-traineeship.component';
import { ShowOneTraineeshipComponent } from './show-one-traineeship/show-one-traineeship.component';


export const TraineeshipRoutes: Routes = [
    {
      path: '',
      children: [ {
        path: 'show',
        component: ShowTraineeshipComponent
    }]} ,{
      path: ':id',
      component:ShowOneTraineeshipComponent
  },
];
