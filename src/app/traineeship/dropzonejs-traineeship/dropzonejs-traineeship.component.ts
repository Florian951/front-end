import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { DropzoneComponent , DropzoneDirective, DropzoneConfigInterface } from 'ngx-dropzone-wrapper';
import { Observable } from 'rxjs';
import { UploadService } from 'src/app/repository/upload.service';
import swal from 'sweetalert2';

@Component({
  selector: 'app-dropzonejs-traineeship',
  templateUrl: './dropzonejs-traineeship.component.html',
  styleUrls: ['./dropzonejs-traineeship.component.css']
})
export class DropzonejsTraineeshipComponent implements OnInit {

  public type: string = 'component';
  documents:any;
  @Input()
  traineeshipId:any;
  // public localStorageToken:string;
  // public disabled: boolean = false;

  public config: DropzoneConfigInterface = {
    headers:  { "Authorization": "Bearer " + localStorage.getItem('token') },
    url: 'http://134.90.137.237/hups-api-rest/public/api/upload',
    acceptedFiles: 'image/*',
    createImageThumbnails: true,
    clickable: true,
    maxFiles: 5,
    autoReset: null,
    errorReset: null,
    cancelReset: null,
    params: {
      traineeship: null
    }
    
  };

  // @ViewChild(DropzoneComponent, { static: false }) componentRef?: DropzoneComponent;
  // @ViewChild(DropzoneDirective, { static: false }) directiveRef?: DropzoneDirective;

  constructor(private repoDocument:UploadService) {}



  ngOnInit() {
    this.repoDocument.findDocument(this.traineeshipId).subscribe(documents => this.documents = documents);
    this.toggleSelectedProject();
 
   
  }
  deleteOneDocument(document) {

    swal({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonClass: 'btn btn-success',
      cancelButtonClass: 'btn btn-danger',
      confirmButtonText: 'Yes, delete it!',
      buttonsStyling: false
    }).then((result) => {
      if (result.value) {
        this.deleteDocument(document.id);
        this.onRemove(document);
      }
    })
  }

  onRemove(document) {
    // this.data.splice(this.data.indexOf(msg), 1);
    // this.currentUser.professionUsers.splice(professionUser, 1);
    const index: number = this.documents.indexOf(document);
    if (index !== -1) {
      this.documents.splice(index, 1);
    }
  };


    deleteDocument(documentId){
      this.repoDocument.deleteDocument(documentId).subscribe();
    }
    consoleTest(){
      console.log(this.traineeshipId);
      
    }
 
  public toggleSelectedProject(): void {
    this.config.params.traineeship = this.traineeshipId;
  }




}
