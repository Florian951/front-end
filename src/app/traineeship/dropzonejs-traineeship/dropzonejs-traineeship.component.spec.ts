import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DropzonejsTraineeshipComponent } from './dropzonejs-traineeship.component';

describe('DropzonejsTraineeshipComponent', () => {
  let component: DropzonejsTraineeshipComponent;
  let fixture: ComponentFixture<DropzonejsTraineeshipComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DropzonejsTraineeshipComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DropzonejsTraineeshipComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
