import { Component, OnInit, Input } from '@angular/core';
import { Address } from 'src/app/entity/Address';
import swal from 'sweetalert2';
import { AuthService } from 'src/app/repository/auth.service';
import { TraineeshipService } from 'src/app/repository/traineeship.service';

@Component({
  selector: 'app-form-address-traineeship',
  templateUrl: './form-address-traineeship.component.html',
  styleUrls: ['./form-address-traineeship.component.css']
})
export class FormAddressTraineeshipComponent implements OnInit {


  departments = [
    {value: 'paris', viewValue: 'Paris'},
    {value: 'miami', viewValue: 'Miami'},
    {value: 'bucharest', viewValue: 'Bucharest'},
    {value: 'new-york', viewValue: 'New York'},
    {value: 'london', viewValue: 'London'},
    {value: 'barcelona', viewValue: 'Barcelona'},
    {value: 'moscow', viewValue: 'Moscow'},
  ];

  regions = [
    {value: 'auvergne-Rhone-alpes', viewValue: 'Auvergne-Rhône-Alpes'},
    {value: 'bourgogne-franche-comté', viewValue: 'Bourgogne-Franche-Comté'},
    {value: 'bretagne', viewValue: 'Bretagne'},
    {value: 'centre-val de loire', viewValue: 'Centre-Val de Loire'},
    {value: 'corse', viewValue: 'Corse'},
    {value: 'grand-est', viewValue: 'Grand Est'},
    {value: 'hauts-de-france', viewValue: 'Hauts-de-France'},
    {value: 'ile-de-france', viewValue: 'Île-de-France'},
    {value: 'normandie', viewValue: 'Normandie'},
    {value: 'nouvelle-aquitaine', viewValue: 'Nouvelle-Aquitaine'},
    {value: 'occitanie', viewValue: 'Occitanie'},
    {value: 'pays-de-la-loire', viewValue: 'Pays de la Loire'},
    {value: "Provence-Alpes-Côte d'Azur", viewValue: 	"Provence-Alpes-Côte d'Azur"},
  ];

  @Input()
  traineeshipAddress: any;
  @Input()
  traineeshipSelected:any;


  newAddress = false;

  address: Address = {

    street: null,
    city: null,
    zipCode: null,
    country: null,
    adressHorse: false,
    department: null,
    region: null,

  };
  message: string;

  constructor(private traineeship:TraineeshipService) { }

  ngOnInit() {

  }

  showSwal() {

    swal({
      title: "Création d'une address reussi!'",
      // text: "You clicked the button!",
      buttonsStyling: false,
      confirmButtonClass: "btn btn-success",
      type: "success"
      
      
    }).catch(swal.noop)
    document.addEventListener("click", function(){
      window.location.reload();
     
    });

  }

  refresh(): void {
    window.location.reload();
  }

  register() {

    this.traineeship.addAddressTraineeship(this.address, this.traineeshipSelected.id).subscribe(() =>
      this.showSwal(),
      data => this.message = data.error.message);

  }
  // patchAddress() {

  //   this.authService.patchAddAddressUser(this.userAddress[0]).subscribe(() =>
  //     this.showSwal(),
  //     data => this.message = data.error.message);

  // }

}
