import { Component, OnInit } from '@angular/core';
import { SearchUserService } from 'src/app/repository/searchUser.service';
import { ActivatedRoute } from '@angular/router';
import { TraineeshipService } from 'src/app/repository/traineeship.service';
import swal from 'sweetalert2';
import { UploadService } from 'src/app/repository/upload.service';


@Component({
  selector: 'app-show-one-traineeship',
  templateUrl: './show-one-traineeship.component.html',
  styleUrls: ['./show-one-traineeship.component.css']
})


export class ShowOneTraineeshipComponent implements OnInit {

  traineeship: any;
  documents:any;

  constructor(private repoSearch: TraineeshipService, private route: ActivatedRoute,private repoDocument:UploadService) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      const id = params['id'];
      this.repoSearch.findOneTraineeship(id).subscribe(traineeship => this.traineeship = traineeship);
      this.repoDocument.findDocument(id).subscribe(documents => this.documents = documents);
    });
   
  }

  consoleTest(){
    console.log(this.documents);
    
  }
   message() {
    swal({
      title: 'Etes vous sûrs?',
      text: 'Si vous publiez votre stage il deviendra visible pour tous et sera impossible à supprimer si une personne souscris',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Oui, publiez!',
      cancelButtonText: 'Non, pas encore',
      confirmButtonClass: "btn btn-success",
      cancelButtonClass: "btn btn-danger",
      buttonsStyling: false
  }).then((result) => {
    if (result.value) {
      swal({
          title: 'Deleted!',
          text: 'Your imaginary file has been deleted.',
          type: 'success',
          confirmButtonClass: "btn btn-success",
          buttonsStyling: false
      }).catch(swal.noop)
    } else {
      swal({
          title: 'Cancelled',
          text: 'Your imaginary file is safe :)',
          type: 'error',
          confirmButtonClass: "btn btn-info",
          buttonsStyling: false
      }).catch(swal.noop)
    }
  })

  }



}
