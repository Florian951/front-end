import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowOneTraineeshipComponent } from './show-one-traineeship.component';

describe('ShowOneTraineeshipComponent', () => {
  let component: ShowOneTraineeshipComponent;
  let fixture: ComponentFixture<ShowOneTraineeshipComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShowOneTraineeshipComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowOneTraineeshipComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
