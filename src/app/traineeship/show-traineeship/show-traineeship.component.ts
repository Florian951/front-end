import { Component, OnInit, Input } from '@angular/core';
import { TableData } from 'src/app/md/md-table/md-table.component';
import { TableData2 } from 'src/app/tables/extendedtable/extendedtable.component';
import { TraineeshipService } from 'src/app/repository/traineeship.service';
import { TestBed } from '@angular/core/testing';

@Component({
  selector: 'app-show-traineeship',
  templateUrl: './show-traineeship.component.html',
  styleUrls: ['./show-traineeship.component.css']
})
export class ShowTraineeshipComponent implements OnInit {

  constructor(private repoTraineeship:TraineeshipService ) { }
  public tableData1: TableData;
  public tableData2: TableData2;
  public tableData3: TableData;
  public traineeships:any;
 
  ngOnInit() {
    this.repoTraineeship.findAllEventByUser().subscribe(traineeship => this.traineeships = traineeship 
    
    )};
  getTotal() {
      let total = 0;
      for (let i = 0; i < this.tableData3.dataRows.length; i++) {
          const integer = parseInt(this.tableData3.dataRows[i][8], 10);
          total += integer;
      }
      return total;
  };

  consoleTest()
  {
    console.log(this.traineeships); 
  } 
  testActive(){
     
  }

}
