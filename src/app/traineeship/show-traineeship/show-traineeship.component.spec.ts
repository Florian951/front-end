import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowTraineeshipComponent } from './show-traineeship.component';

describe('ShowTraineeshipComponent', () => {
  let component: ShowTraineeshipComponent;
  let fixture: ComponentFixture<ShowTraineeshipComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShowTraineeshipComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowTraineeshipComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
