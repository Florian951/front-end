// IMPORTANT: this is a plugin which requires jQuery for initialisation and data manipulation

import { Component, OnInit } from '@angular/core';
import { DateAdapter } from '@angular/material';
import swal from 'sweetalert2';
import PerfectScrollbar from 'perfect-scrollbar';
import frLocale from '@fullcalendar/core/locales/fr';
import { EventLessonService } from '../repository/event-lesson.service';
import { AuthService } from '../repository/auth.service';
import { CalendarEvent } from '../entity/CalendarEvent';
import { Moment } from 'moment';


declare const $: any;

@Component({
    selector: 'app-calendar-cmp',
    templateUrl: 'calendar.component.html'
})

export class CalendarComponent implements OnInit {

    currentUser: any;
    events: any;
    currentCity: string[];
    // selected: {start: Moment, end: Moment};
    selected: any;




    selectTheme = 'primary';
    cities = [
        { value: 'paris-0', viewValue: 'Paris' },
        { value: 'miami-1', viewValue: 'Miami' },
        { value: 'bucharest-2', viewValue: 'Bucharest' },
        { value: 'new-york-3', viewValue: 'New York' },
        { value: 'london-4', viewValue: 'London' },
        { value: 'barcelona-5', viewValue: 'Barcelona' },
        { value: 'moscow-6', viewValue: 'Moscow' },
    ];


    constructor(private repoEvent: EventLessonService, private repoAuth: AuthService) { }
    isInvalidDate(date) {
        return date.weekday() === 0;
    };
    testConsole() {
        console.log(this.selected);

    }
    ngOnInit() {
        let that = this;
        let eventsArray = [];

        this.repoEvent.findAllEventByUser().subscribe(events => {

            that.events = events
            console.log(JSON.stringify(that.events));

            that.events.forEach(function (value, index) {

                console.log(value.id);

                let calendarEvent = new CalendarEvent();
                calendarEvent.title = value.title;
                calendarEvent.start = value.start;
                calendarEvent.end = value.end;
                calendarEvent.className = value.className;
                console.log(that);

                eventsArray.push(calendarEvent);
            });


            that.events = eventsArray;
            console.log(JSON.stringify(that.events));


            this.repoAuth.getUser().subscribe(() => this.loadUser()
            );

            const $calendar = $('#fullCalendar');


            const today = new Date();
            const y = today.getFullYear();
            const m = today.getMonth();
            const d = today.getDate();
            let token;


            $calendar.fullCalendar({

                lang: 'fr',
                // locales: [frLocale ],
                // locale: 'fr',
                viewRender: function (view: any, element: any) {

                    // We make sure that we activate the perfect scrollbar when the view isn't on Month
                    if (view.name != 'month') {
                        var elem = $(element).find('.fc-scroller')[0];
                        let ps = new PerfectScrollbar(elem);
                    }

                },



                header: {
                    left: 'title',
                    center: 'month, agendaWeek, agendaDay',
                    right: 'prev, next, today'
                },
                businessHours: {
                    // days of week. an array of zero-based day of week integers (0=Sunday)
                    daysOfWeek: [1, 2, 3, 4, 5], // Monday - Thursday

                    startTime: '7:00', // a start time (10am in this example)
                    endTime: '18:00', // an end time (6pm in this example)
                },
                defaultDate: today,

                selectable: true,
                selectHelper: true,
                views: {
                    month: { // name of view
                        titleFormat: 'MMMM YYYY'
                        // other view-specific options here
                    },
                    week: {
                        titleFormat: 'D MMMM, YYYY'
                    },
                    day: {
                        titleFormat: 'D MMMM, YYYY'
                    }
                },

                select: function (start: any, end: any) {

                    function getToken() {
                        return window.localStorage.getItem('token');
                    }

                    function authenticate(eventCalendarByPro) {

                        token = window.localStorage.getItem('token');
                        $.ajax
                            ({
                                type: "POST",
                                //the url where you want to sent the userName and password to
                                url: 'http://134.90.137.237/hups-api-rest/public/api/add-event',

                                headers: {
                                    'Authorization': 'bearer ' + token
                                },

                                dataType: 'json',
                                contentType: 'application/json',
                                async: false,
                                //json object to sent to the authentication url
                                data: JSON.stringify(eventCalendarByPro),

                                // beforeSend: function(xhr, settings) { xhr.setRequestHeader('Authorization','Bearer ' + token); } 
                            })
                    }

                    swal({

                        title: 'Ajouter un titre',
                        html: '<div class="form-group">' +
                            '<input class="form-control" placeholder="Event Title" id="input-field">' +
                            '</div>'
                        ,
                        customClass: 'swal2-overflow',

                        showCancelButton: true,
                        confirmButtonClass: 'btn btn-success',
                        cancelButtonClass: 'btn btn-danger',
                        buttonsStyling: false
                    }).then(function (result: any) {

                        let eventCalendarByPro: any;
                        const event_title = $('#input-field').val();

                        if (event_title) {
                            eventCalendarByPro = {
                                title: event_title,
                                start: start,
                                end: end,
                                className: 'event-rose'
                            };
                            $calendar.fullCalendar('renderEvent', eventCalendarByPro, true); // stick? = true 
                            authenticate(eventCalendarByPro);
                            getToken

                        }
                        $calendar.fullCalendar('unselect');


                    });

                },
                minTime: '07:00:00',
                maxTime: '18:00:00',

                editable: false,
                eventLimit: true, // allow "more" link when too many events


                // color classes: [ event-blue | event-azure | event-green | event-orange | event-red ]
                events: eventsArray,
                //  [


                //      {
                //          title: 'Birthday Party',
                //          start: new Date(y, m, d + 1, 19, 0),
                //          end: new Date(y, m, d + 1, 22, 30),
                //          allDay: false,
                //          className: 'event-azure'
                //      }
                //      {
                //          title: 'Click for Creative Tim',
                //          start: new Date(y, m, 21),
                //          end: new Date(y, m, 22),
                //          url: 'https://www.creative-tim.com/',
                //          className: 'event-orange'
                //      },
                //   
                //  ]

            });

        }
        );

    }
    postNewEvent(eventCalendarByPro) {
        this.repoEvent.addEventForProfessionnal(eventCalendarByPro).subscribe()
    };


    loadUser() {
        this.repoAuth.user.subscribe(user => this.currentUser = user);
    }


}
