import { Routes } from '@angular/router';
import { FormCompetencesComponent } from './form-competences/form-competences.component';
import { ShowCompetencesComponent } from './show-competences/show-competences.component';


export const CompetencesRoutes: Routes = [
    {
      path: '',
      children: [ {
        path: 'show',
        component: ShowCompetencesComponent
    }]}, {
    path: '',
    children: [ {
      path: 'edit',
      component: FormCompetencesComponent
    }]
    }
];
