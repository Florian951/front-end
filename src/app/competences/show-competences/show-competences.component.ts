import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/repository/auth.service';
import { User } from 'src/app/entity/User';
import { CompetencesService } from 'src/app/repository/competences.service';
import swal from 'sweetalert2';

declare interface TableData {
  headerRow: string[];
  dataRows: string[][];
}

@Component({
  selector: 'app-show-competences',
  templateUrl: './show-competences.component.html',
  styleUrls: ['./show-competences.component.css']
})

export class ShowCompetencesComponent implements OnInit {
  currentUser: User;
  editSelected = false;
  professions: any;
  selected: any;
  public tableData1: TableData;

  constructor(private authRepo: AuthService, private repoCompetences: CompetencesService) { }

  ngOnInit() {

    this.repoCompetences.findAllProfessionForPro().subscribe(data => this.professions = data);
    this.authRepo.getUser().subscribe(() => this.loadUser()

    );
  }

  deleteProfessionUser(professionUser) {

    swal({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonClass: 'btn btn-success',
      cancelButtonClass: 'btn btn-danger',
      confirmButtonText: 'Yes, delete it!',
      buttonsStyling: false
    }).then((result) => {
      if (result.value) {
        this.deleteprofessionSelected(professionUser.id);
        this.onRemove(professionUser);
      }
    })
  }

  deleteprofessionSelected(professionUser) {

    this.repoCompetences.deleteCompetenceByProfessionnal(professionUser).subscribe(

      () => swal(

        {
          title: 'Deleted!',
          text: 'Your file has been deleted.',
          type: 'success',
          confirmButtonClass: "btn btn-success",
          buttonsStyling: false
        }
      )
    )
  }

  onRemove(professionUser) {
    // this.data.splice(this.data.indexOf(msg), 1);
    // this.currentUser.professionUsers.splice(professionUser, 1);
    const index: number = this.currentUser.professionUsers.indexOf(professionUser);
    if (index !== -1) {
      this.currentUser.professionUsers.splice(index, 1);
    }
  };

  editActive() {
    if (this.editSelected == false) {
      this.editSelected = true;
    } else {
      this.editSelected = false;
    }
  }

  loadUser() {
    this.authRepo.user.subscribe(user => this.currentUser = user);
  }
}
