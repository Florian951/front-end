import { Component, OnInit, Input } from '@angular/core';
import { Profession } from 'src/app/entity/Profession';
import { ProfessionUser } from 'src/app/entity/ProfessionUser';
import { CompetencesService } from 'src/app/repository/competences.service';
import swal from 'sweetalert2';


@Component({
  selector: 'app-form-competences',
  templateUrl: './form-competences.component.html',
  styleUrls: ['./form-competences.component.css']
})
export class FormCompetencesComponent implements OnInit {

  professionForm:ProfessionUser = {
   
    price:null,
    hour:null,
    description:null,
    professions:null
    
  }
  professionId:number;
  message:any;
  
  @Input()
  profession:any;
  
  // activated = false;
  priceSlider = 50 ;

  hoursConfig:any;
  professionConfig:any;
  
  constructor(private repoCompetences:CompetencesService) { }

  consoleTest(){
    console.log(this.profession);
  }


  addProfession(){
    this.professionForm.price = this.priceSlider;
    this.professionId = this.profession.id
   
    this.repoCompetences.AddProfessionPro(this.professionForm, this.professionId).subscribe(() =>
    this.showSwal(),
    data => this.message = data.error.message);
  }

  showSwal() {

    swal({
      title: "Création d'une compétence reussi!'",
      // text: "You clicked the button!",
      buttonsStyling: false,
      confirmButtonClass: "btn btn-success",
      type: "success"
      
      
    }).catch(swal.noop)
    document.addEventListener("click", function(){
      
       window.location.reload();
     
    });

  }


 test(){

    //  for (const iterator of this.profession.optionHour) {        
    //     this.hoursConfig = iterator ;
        
        
    //   };
    //   for (const iterator of this.profession.optionProfession) {        
    //     this.professionConfig = iterator 
    //   };
     
     

    this.hoursConfig = [
      this.profession.optionHour
    ];
    this.professionConfig = [
      this.profession.optionProfession
    ];
   
  }
  ngOnInit() {
    this.test();

  }

}
