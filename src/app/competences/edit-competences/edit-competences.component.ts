import { Component, OnInit,Input } from '@angular/core';
import { CompetencesService } from 'src/app/repository/competences.service';

@Component({
  selector: 'app-edit-competences',
  templateUrl: './edit-competences.component.html',
  styleUrls: ['./edit-competences.component.css']
})
export class EditCompetencesComponent implements OnInit {

  rider: string = "Ajouter";
  podiatrist: string = "Ajouter";
  veterinary: string = "Ajouter";
  osteopath: string = "Ajouter";
  ethologist: string = "Ajouter";
  instructor: string = "Ajouter";
  teacher: string = "Ajouter";
  
  idSelected:any;

  @Input()
  professions:any;

  constructor( ) { }

  ngOnInit() {
  }

  openForm(id){
   this.idSelected = id;
    
  };

 

}
