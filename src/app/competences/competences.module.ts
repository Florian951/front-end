import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NouisliderModule } from 'ng2-nouislider';
import { TagInputModule } from 'ngx-chips';
import { SelectModule } from 'ng2-select';
import { MaterialModule } from '../app.module';
import { CompetencesRoutes } from './competences.routing';


import { FormCompetencesComponent } from './form-competences/form-competences.component';
import { ShowCompetencesComponent } from './show-competences/show-competences.component';
import { EditCompetencesComponent } from './edit-competences/edit-competences.component';



@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(CompetencesRoutes),
    FormsModule,
    ReactiveFormsModule,
    NouisliderModule,
    TagInputModule,
    MaterialModule
  ],
  declarations: [
     
     FormCompetencesComponent,
     ShowCompetencesComponent,
     EditCompetencesComponent
  ]
})

export class CompetencesModule {}
